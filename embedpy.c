#include <stdio.h>
#include <stdlib.h>
#include <Python.h>

int setup_searching_path()
{
    PyObject *pypath_list, *pycurrent_path;
    int exit_code;

    pypath_list = PySys_GetObject("path");
    pycurrent_path = PyString_FromString("");

    if (pypath_list && pycurrent_path)
    {
        PyList_Insert(pypath_list, 0, pycurrent_path);
        Py_DECREF(pycurrent_path);

        if (PyErr_Occurred())
        {
            PyErr_Print();
            exit_code = -1;
        }
        else
        {
            exit_code = 0;
        }
    }
    else
    {
        if (PyErr_Occurred())
            PyErr_Print();
        fprintf(stderr, "\n# failed to change library searching path #\n");
        exit_code = -1;
    }

    return exit_code;
}

int _main()
{
    char module_name[255], func_name[255];
    PyObject *pymodule_name, *pymodule, *pydict, *pyfunc;
    PyObject *pyargs, *pyvalue;
    PyObject *pydump;
    int exit_code;

    /* get input */
    memset(module_name, 0, 255);
    memset(func_name, 0, 255);
    printf("\npython module name: ");
    scanf("%s", module_name);
    printf("\npython function name: ");
    scanf("%s", func_name);

    /* import module */
    exit_code = setup_searching_path();
    if (exit_code != 0)
        return exit_code;
    pymodule_name = PyString_FromString(module_name);
    pymodule = PyImport_Import(pymodule_name);

    /* release module name object */
    Py_DECREF(pymodule_name);

    if (pymodule)
    {
        pyfunc = PyObject_GetAttrString(pymodule, func_name);

        if (pyfunc && PyCallable_Check(pyfunc))
        {
            /* build calling arguments */
            pyargs = PyTuple_New(1);
            pyvalue = PyString_FromString("world");

            /* reference stolen here so do not need to change the ref count */
            PyTuple_SetItem(pyargs, 0, pyvalue);

            /* call the function and release used arguments tuple object */
            pyvalue = PyObject_CallObject(pyfunc, pyargs);
            Py_DECREF(pyargs);

            if (pyvalue)
            {
                /* output resule and release reference */
                pydump = PyObject_Repr(pyvalue);
                Py_DECREF(pyvalue);
                printf("\n========== Python Execution Output ==========\n");
                printf("%s", PyString_AsString(pydump));
                printf("\n=============================================\n");
                Py_DECREF(pydump);
                exit_code = 0;
            }
            else
            {
                if (PyErr_Occurred())
                    PyErr_Print();
                fprintf(stderr, "\n# found function but failed to call #\n");
                exit_code = -1;
            }
        }
        else
        {
            if (PyErr_Occurred())
                PyErr_Print();
            fprintf(stderr, "\n# failed to load function '%s' #\n", func_name);
            exit_code = -1;
        }

        /* release module object and function object */
        Py_XDECREF(pyfunc);
        Py_DECREF(pymodule);
    }
    else
    {
        if (PyErr_Occurred())
            PyErr_Print();
        fprintf(stderr, "\n# failed to import module '%s' #\n", module_name);
        exit_code = -1;
    }

    return exit_code;
}

int main(int argc, char **argv)
{
    int exit_code;

    Py_SetProgramName(argv[0]);
    Py_Initialize();

    exit_code = _main();

    Py_Finalize();

    return exit_code;
}
