#!/usr/bin/env bash

project_root="$(pwd)"
build_root="${project_root}/build"

(
    rm -rf "${build_root}" &>/dev/null
    mkdir -p "${build_root}"
    cd "${build_root}" && cmake ${project_root} && make
)
